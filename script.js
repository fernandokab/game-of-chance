let userCount = 0;
let machineCount = 0;
document.querySelector('.userCount').textContent = userCount;
document.querySelector('.machineCount').textContent = machineCount;

function jokenpo(){
  let numMachine = Math.floor(Math.random()*3)+1
  if (numMachine === 1) {
    document.querySelector('.machineSelection').innerHTML = '<i class="fas fa-hand-rock"></i>'
  }
  if (numMachine === 2) {
    document.querySelector('.machineSelection').innerHTML = '<i class="fas fa-hand-sparkles"></i>'
  }
  if (numMachine === 3){
    document.querySelector('.machineSelection').innerHTML = '<i class="fas fa-hand-scissors" id="tesoura"></i>'
  }

  let typeId = this.getAttribute('id')
  
  if (typeId === 'pedra' && numMachine === 1 || typeId === 'papel' && numMachine === 2 || typeId === 'tesoura' && numMachine === 3){
    document.querySelector('.result').textContent = ''
    document.querySelector('.result').textContent = 'Empate!'
  }

  if (typeId === 'pedra' && numMachine === 2 ||typeId === 'papel' && numMachine === 3 || typeId === 'tesoura' && numMachine === 1){
    document.querySelector('.result').textContent = ''
    document.querySelector('.result').textContent = 'A máquina venceu!'
    machineCount++
    document.querySelector('.machineCount').textContent = machineCount
  }

  if (typeId === 'pedra' && numMachine === 3 || typeId === 'papel' && numMachine === 1 || typeId === 'tesoura' && numMachine === 2){
    document.querySelector('.result').textContent = ''
    document.querySelector('.result').textContent = 'Você venceu!'
    userCount++
    document.querySelector('.userCount').textContent = userCount
  }

}

let choice = document.querySelectorAll('.ppt')

for (let i = 0; i < 3; i++){
  choice[i].addEventListener('click', jokenpo)
}

function resetGame(){
  userCount = 0;
  machineCount = 0;
  document.querySelector('.userCount').textContent = userCount;
  document.querySelector('.machineCount').textContent = machineCount;
  document.querySelector('.result').textContent = '';
  document.querySelector('.machineSelection').innerHTML = ''
}

document.querySelector('.resetPPT').onclick = function(){resetGame()};

let arrayOfAnswer = [
    'Com certeza!',
    'Está decido que assim será',
    'Sem duvidas',
    'Sim, definitivamente sim',
    'Confia no pai e vai',
    'Pirilim salibm, pode crer que sim',
    'Bem provável',
    'Parece que vai dar bom',
    'Sim',
    'Os cosmos apontam para sim...',
    'Boa questão... porém eu não sei',
    'Assim como o sol raia no amanhecer, a sua respota chegará',
    'Você não vai gostar da resposta',
    'Quem sabe né!?',
    'A resposta está dentro de você',
    'Tire o cavalinho da chuva',
    'Digo que não',
    'Me contaram que a respota é não',
    'Nem em séculos',
    'Batatinha quando nasce, se esparrama pelo chão, a reposta para sua pergunta é não.'
  ]

function ballmagic() {
  let numBall = Math.floor(Math.random()*20)+1;
  document.querySelector('.ballAnswer').textContent = ''
  document.querySelector('.ballAnswer').textContent = arrayOfAnswer[numBall]

}

document.querySelector('.respondame').onclick = function(){ballmagic()};

function slotMachine() {
  document.querySelector('.animation').removeAttribute('disabled');
  document.querySelector('.sorte').setAttribute('disabled', '')
  let desanimar = document.querySelectorAll('.slot');
  for (i = 0; i < 3; i++){
    desanimar[i].setAttribute('class', `slot${i+1} slot`)
  }
  
  let numSlot1 = Math.floor(Math.random()*7)+1;
  let numSlot2 = Math.floor(Math.random()*7)+1;
  let numSlot3 = Math.floor(Math.random()*7)+1;

  let money = '<i class="fas fa-dollar-sign"></i>';
  let pepper = '<i class="fas fa-pepper-hot"></i>';
  let apple = '<i class="fas fa-apple-alt"></i>';
  let anchor = '<i class="fas fa-anchor"></i>';
  let leaf = '<i class="fab fa-canadian-maple-leaf"></i>';
  let crown =  '<i class="fas fa-crown"></i>';
  let heart = '<i class="fas fa-heart"></i>';


  if (numSlot1 === 1) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = money
  }

  if (numSlot1 === 2) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = pepper
  }

  if (numSlot1 === 3) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = apple
  }

  if (numSlot1 === 4) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = anchor
  }

  if (numSlot1 === 5) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = leaf
  }

  if (numSlot1 === 6) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = crown
  }

  if (numSlot1 === 7) {
    document.querySelector('.slot1').innerHTML = ''
    document.querySelector('.slot1').innerHTML = heart
  }

 //SLOT2

  if (numSlot2 === 1) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = money
  }

  if (numSlot2 === 2) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = pepper
  }

  if (numSlot2 === 3) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = apple
  }

  if (numSlot2 === 4) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = anchor
  }

  if (numSlot2 === 5) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = leaf
  }

  if (numSlot2 === 6) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = crown
  }

  if (numSlot2 === 7) {
    document.querySelector('.slot2').innerHTML = ''
    document.querySelector('.slot2').innerHTML = heart
  }

  //SLOT3

  if (numSlot3 === 1) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = money
  }

  if (numSlot3 === 2) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = pepper
  }

  if (numSlot3 === 3) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = apple
  }

  if (numSlot3 === 4) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = anchor
  }

  if (numSlot3 === 5) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = leaf
  }

  if (numSlot3 === 6) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = crown
  }

  if (numSlot3 === 7) {
    document.querySelector('.slot3').innerHTML = ''
    document.querySelector('.slot3').innerHTML = heart
  }

  //  RESULTADO

  document.querySelector('.resultSlot').textContent = '';
  document.querySelector('.resultSlot').textContent = 'Você perdeu "(';

  if(numSlot1 === numSlot2 && numSlot1 === numSlot3){
    document.querySelector('.resultSlot').textContent = '';
    document.querySelector('.resultSlot').textContent = 'Parábens, você ganhou';
  }
}

document.querySelector('.sorte').onclick = function(){slotMachine()};

function animation() {
  let animar = document.querySelectorAll('.slot');
  for (let i = 0; i < 3; i++){
    animar[i].setAttribute('class', `slot${i+1} slot test${i+1}`);
    document.querySelector(`.slot${i+1}`).innerHTML = ''
  }
  document.querySelector('.sorte').removeAttribute('disabled');
  document.querySelector('.animation').setAttribute('disabled','')

}

document.querySelector('.animation').onclick = function() {animation()};